#include <iostream>
#define MAX 10
using namespace std;
class poly
{
    int n;
    int coef[MAX],pol[MAX];

    public:
        friend ostream & operator << (ostream &out, const poly &c);
    friend istream & operator >> (istream &in,  poly &c);
        void getdata()
        {
            int i=0;

            cout<<"\n Enter the highest degree of polynomial n :";
            cin>>n;

            cout<<"\n Enter "<<n+1<<" coefficients :";
            for(i=0; i<n+1; i++)
                cin>>coef[i];
        }

        void printdata()
        {
            int i=0;
            for(i=n; i>0; i--)
            cout<<coef[i]<<"X^"<<i<<" + ";
            cout<<coef[0]<<endl;

        }

        poly operator + (poly p)
        {
            poly temp;
            int i=0;
            if(n > p.n)
            {
                temp.n = n;
                for(i=n; i>p.n; i--)
                    temp.coef[i] = coef[i];
                for(i=p.n; i>=0; i--)
                    temp.coef[i] = coef[i] + p.coef[i];
                return temp;
            }
            else if(n < p.n)
            {
                temp.n = p.n;
                for(i=p.n; i>n; i--)
                    temp.coef[i] = p.coef[i];
                for(i=n; i>=0; i--)
                    temp.coef[i] = coef[i] + p.coef[i];
                return temp;
            }
            else
            {
                temp.n = n;
                for(i=n; i>=0; i--)
                    temp.coef[i] = coef[i] + p.coef[i];
                return temp;
            }
        }

        poly operator - (poly p)
        {
            poly temp;
            int i=0;
            if(n > p.n)
            {
                temp.n = n;
                for(i=n; i>p.n; i--)
                    temp.coef[i] = coef[i];
                for(i=p.n; i>=0; i--)
                    temp.coef[i] = coef[i] - p.coef[i];
                return temp;
            }
            else if(n < p.n)
            {
                temp.n = p.n;
                for(i=p.n; i>n; i--)
                    temp.coef[i] = p.coef[i];
                for(i=n; i>=0; i--)
                    temp.coef[i] = coef[i] - p.coef[i];
                return temp;
            }
            else
            {
                temp.n = n;
                for(i=n; i>=0; i--)
                    temp.coef[i] = coef[i] - p.coef[i];
                return temp;
            }
        }

        poly operator * (int c)
        {
            poly temp;
            int i;

            temp.n = n;
            for(i=n; i>=0 ; i--)
                temp.coef[i]=c*coef[i];
            return temp;
        }
};


int main()
{
    poly p1,p2,addp3,subp4,mulp5;
    p1.getdata();
    p2.getdata();
    cout<<"polynomial 1 :"<<endl;
    p1.printdata();
    cout<<"polynomial 2 :"<<endl;
    p2.printdata();

    addp3 = p1 + p2;
    cout<<"Sum of polynomials :"<<endl;
    addp3.printdata();

    subp4 = p1 - p2;
    cout<<"Subtraction of polynomials:"<<endl;
    subp4.printdata();

    int c;
        cout<<" Enter the value of scalar value c:";
        cin>>c;
    mulp5 = p1*c;


    cout<<"Multiplied polynomial 1:"<<endl;
    mulp5.printdata();


    return 0;
}
