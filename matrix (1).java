import java.util.Scanner;
class matrix
{
	 public static void main(String args[])
    {
        Matrix_create obj=new Matrix_create();
        obj.create();
        obj.display();
        obj.add();
        obj.transpose();
        obj.multiplication();
        obj.scalar();
    }

}

class Matrix_create
{
    int matrix1[][],matrix2[][],c[][],tran_1[][],s[][];
    int row,column,i,j,k,sum;

    Matrix_create()
    {
    	row=0;
    	column=0;
    	sum=0;
    }

    void create()
    {
     Scanner s=new Scanner(System.in);

    System.out.println("matrix creation");
    //first matrix creation
    System.out.println("enter number of rows:");
    row=s.nextInt();

    System.out.println("enter number of columns:");
    column=s.nextInt();

    matrix1=new int[row][column];
    matrix2=new int[row][column];
    c=new int[row][column];


    System.out.println("enter data for first matrix:");
    for(i=0;i<row;i++)
    {
        for(j=0;j<column;j++)
        {
            matrix1[i][j]=s.nextInt();
        }
    }
    //second matrix creation
    System.out.println("enter data for second matrix:");
    for(i=0;i<row;i++)
    {
        for(j=0;j<column;j++)
        {
            matrix2[i][j]=s.nextInt();
        }
    }
 }

    void display()
    {
        System.out.println("\n first matrix is:");

        for(i=0;i<row;i++)
        {
            for(j=0;j<column;j++)
            {
                System.out.print("\t"+matrix1[i][j]);
            }
            System.out.println("\n");
        }
        System.out.println("\n \n second matrix is:");

        for(i=0;i<row;i++)
        {
            for(j=0;j<column;j++)
            {
                System.out.print("\t"+matrix2[i][j]);
            }
            System.out.println("\n");
    }
   }


 void add()
 {
	 for(i=0;i<row;i++)
	 {
		 for(j=0;j<column;j++)
		 {
			 c[i][j]=matrix1[i][j]+matrix2[i][j];
		 }
	 }
	 System.out.println("\n \n the sum is:");

	 for(i=0;i<row;i++)
	 {
		 for(j=0;j<column;j++)
		 {
			 System.out.print("\t"+c[i][j]);
		 }
		 System.out.println("\n");
	 }
 }

 void multiplication()
 {
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			for(k=0;k<column;k++)
			{
			sum+=matrix1[i][k]*matrix2[k][j];
		    }
			c[i][j]=sum;
			sum=0;
	}
 }
	System.out.println("\n product of entered matrices:");
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			System.out.print("\t"+c[i][j]);
		}
		System.out.println("\n");
    }
 }
void transpose()
	{
	{
	    int tran_1[][] = new int[row][column];


		for(i=0;i<row;i++)
		 {
			 for(j=i+1;j<column;j++)
			 {
				 tran_1[i][j]=matrix1[i][j];;
				 matrix1[i][j]=matrix1[j][i];
				 matrix1[j][i]=tran_1[i][j];
			 }
			 System.out.println("\n");
		 }
		System.out.println("transpose matrix1:");
		for(i=0;i<row;i++)
		 {
			 for(j=0;j<column;j++)
			 {
				 System.out.print("\t"+matrix1[i][j]);
			 }
			 System.out.println("\n");
		 }

		}
	}
	void scalar()
 {
     int val;
     cout<<"enter a scalar value: "<<endl;
     cin<<val;
	 for(i=0;i<row;i++)
	 {
		 for(j=0;j<column;j++)
		 {
			 s[i][j]=matrix1[i][j]*val;
		 }
	 }
	 System.out.println("\n \n the matrix 1 is:");

	 for(i=0;i<row;i++)
	 {
		 for(j=0;j<column;j++)
		 {
			 System.out.print("\t"+s[i][j]);
		 }
		 System.out.println("\n");
	 }
 }

}
